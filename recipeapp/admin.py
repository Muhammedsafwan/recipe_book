from django.contrib import admin
from .models import Recipe,Category

class RecipeAdmin(admin.ModelAdmin):
    list_display = ('title', 'ingredients', 'category','image')  # Fields to display in the list view
    # list_filter = ('created_at', 'updated_at')  # Filter options in the right sidebar
    # search_fields = ('title', 'ingredients')  # Search functionality
    # prepopulated_fields = {'slug': ('title',)}  # Automatically populate the 'slug' field based on the 'title'

# Register the Recipe model and the RecipeAdmin class with the admin site
admin.site.register(Recipe, RecipeAdmin)
admin.site.register(Category)
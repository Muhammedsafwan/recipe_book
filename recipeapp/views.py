from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Recipe, Category
from .forms import RecipeForm
from django.views.decorators.http import require_POST


def HomePage(request):
    return render(request, "homepage.html")


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipe_list.html"
    context_object_name = "recipes"

    def get_queryset(self):
        queryset = super().get_queryset()
        category_id = self.request.GET.get("category")
        if category_id:
            queryset = queryset.filter(category=category_id)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_id = self.request.GET.get("category")
        categories = Category.objects.all()
        for i in categories:
            print("type id", type(i))
        context[
            "categories"
        ] = Category.objects.all()  # Ensure you retrieve all categories
        context["category_id"] = category_id
        print("category id", type(category_id))
        return context


class RecipeAddView(CreateView):
    model = Recipe
    template_name = "recipe_add.html"
    fields = ["title", "category", "ingredients", "instructions", "image"]
    success_url = reverse_lazy(
        "recipe_list"
    )  # Define a success URL (customize as needed)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        return context


class RecipeEditView(UpdateView):
    model = Recipe
    form_class = RecipeForm  # Use the RecipeForm for the view
    template_name = "recipe_edit.html"
    success_url = reverse_lazy(
        "recipe_list"
    )  # Redirect to the listing page after editing

    def get_object(self, queryset=None):
        return get_object_or_404(Recipe, pk=self.kwargs.get("pk"))

    def form_valid(self, form):
        # Handle form submission here (e.g., save the form data)
        # You can add any additional logic you need for form submission
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["categories"] = Category.objects.all()  # Add categories to the context
        return context


class RecipeDeleteView(DeleteView):
    model = Recipe
    success_url = reverse_lazy("recipe_list")


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipe_single.html"
    context_object_name = "recipe"


# @require_POST
# def delete_recipe(request, recipe_id):
#     try:
#         recipe = Recipe.objects.get(pk=recipe_id)
#         recipe.delete()
#         return JsonResponse({"success": True})
#     except Recipe.DoesNotExist:
#         return JsonResponse({"success": False, "error": "Recipe not found"})
#     except Exception as e:
#         return JsonResponse({"success": False, "error": str(e)})


# class RecipeEditView(UpdateView):
#     model = Recipe
#     template_name = 'recipe_edit.html'
#     fields = ['title', 'ingredients', 'instructions', 'category', 'image']
#     success_url = reverse_lazy('recipe_list')  # Redirect to the listing page after editing

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['categories'] = Category.objects.all()  # Add categories to the context
#         return context

# class RecipeAddView(CreateView):
#     model = Recipe
#     template_name = 'recipe_add.html'
#     fields = ['title', 'ingredients', 'instructions', 'image']
#     success_url = '/success/'  # Define a success URL (you can customize this)

# class RecipeListView(ListView):
#     model = Recipe
#     template_name = "recipe_list.html"
#     context_object_name = "recipes"

#     def get_queryset(self):
#         queryset = super().get_queryset()
#         category_id = self.request.GET.get("category")
#         if category_id:
#             queryset = queryset.filter(category=category_id)
#         return queryset

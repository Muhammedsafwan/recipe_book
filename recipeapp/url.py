from django.urls import path
from . import views

urlpatterns = [
    path("", views.HomePage, name="home"),
    path("recipe-list", views.RecipeListView.as_view(), name="recipe_list"),
    path("recipe/<int:pk>/", views.RecipeDetailView.as_view(), name="recipe_detail"),
    path("add/", views.RecipeAddView.as_view(), name="recipe_add"),
    path("edit/<int:pk>/", views.RecipeEditView.as_view(), name="recipe_edit"),
    path(
        "recipe/<int:pk>/delete/",
        views.RecipeDeleteView.as_view(),
        name="recipe_delete",
    ),
    # path("delete_recipe/<int:recipe_id>/", views.delete_recipe, name="delete_recipe"),
    # path('hello/', views.hello_world, name='hello_world'),
]
